terragrunt = {
  include {
    path = "${find_in_parent_folders()}"
  }

  terraform {
    source = "/home/saiteja/terragrunt_gcs/modules/gcs"
  }
}
gcs_name = "preprodbuck1"
gcs_storage_class = "REGIONAL"
gcs_location = "us-east1"
