terragrunt = {
  remote_state {
    backend = "gcs"
    config {
      bucket         = "preprodenv1"
      prefix         = "${path_relative_to_include()}"
      region         = "europe-west1"
      project        = "kubernetes-218409"
    }
  }
}
