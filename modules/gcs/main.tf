terraform {
  backend "gcs" {}
}


provider "google" {
      project     = "kubernetes-218409"
}

resource "google_storage_bucket" "image-store" {
  name          = "${var.gcs_name}"
  storage_class = "${var.gcs_storage_class}"
  location      = "${var.gcs_location}"
}
