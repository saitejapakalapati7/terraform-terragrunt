variable gcs_name{
    default = "bucket12s"
}
variable gcs_storage_class{
    default = "REGIONAL"
}
variable gcs_location{
    default = "us-east1"
}